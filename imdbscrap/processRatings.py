import json
import json
import re

"""
Modifie les datas scrappées du fichier 'ratings.json' puis les enregistre dans le fichier 'ratingsProcessed.json'.
Utilisation : python processRatings.py
"""

# Sépare dans deux arrays les pourcentages et le nombre de votes 
# pour chaque note allant de 1 à 10, et les convertis en nombres
def process_ratings(data):
    pourcentagesNotes1to10 = []
    nbVotesNotes1to10 = []
    for i in range(1, 11):
        note_key = f'note{i}'
        note_value = data.get(note_key)
        if note_value:
            percentage = float(note_value.split('%')[0])
            number_match = re.search(r'\((\d+(\.\d+)?)([MK]?)\)', note_value)
            if number_match:
                number_str = number_match.group(1)
                modifier = number_match.group(3)
                if modifier == 'M':
                    number = float(number_str) * 1000000
                    number = float(number_str) * 1000000
                elif modifier == 'K':
                    number = float(number_str) * 1000
                    number = float(number_str) * 1000
                else:
                    number = float(number_str)
                pourcentagesNotes1to10.append(percentage)
                nbVotesNotes1to10.append(number)
    return pourcentagesNotes1to10, nbVotesNotes1to10

# Charge les datas du fichier 'ratings.json' dans ratings_data
with open('ratings.json') as f:
    ratings_data = json.load(f)

# Traite tous les objets ratings_data dans une boucle
processed_data = []
for obj in ratings_data:
    pourcentages, nbVotes = process_ratings(obj)
    obj['pourcentagesNotes1to10'] = pourcentages
    obj['nbVotesNotes1to10'] = nbVotes

    # Retire les entrées maintenant inutiles note10 à note1
    for i in range(1, 11):
        key = f'note{i}'
        obj.pop(key, None)
    
    # Conversion en nombre de "imdbRating"
    imdbRating_number = float(obj['imdbRating'])
    obj['ratingImdb'] = imdbRating_number
    obj.pop('imdbRating', None) 

    # Renommage de "unweightedMean" en "unweightedMeanImdb" et conversion en nombre
    unweighted_mean_str = obj['unweightedMean']
    unweighted_mean_number = float(unweighted_mean_str.split()[0])
    obj['unweightedMeanImdb'] = unweighted_mean_number
    obj.pop('unweightedMean', None)
    processed_data.append(obj)

# Sauvegarde les data traitées dans ratingsProcessed.json
# NB : "ensure_ascii=False" pour gérer e.g. les accents français
with open('ratingsProcessed.json', 'w') as f:
    json.dump(processed_data, f, indent=2, ensure_ascii=False)