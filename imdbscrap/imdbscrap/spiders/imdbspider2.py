import scrapy
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
import json

"""
Utilise Scrapy avec Selenium pour manipuler le navigateur Chrome pour scraper entre autres 
le nombre de votes pour chaque note donnée à chaque film du top250 de l'Internet Movie Database.
NB : utilisation pour sauvegarder le yield dans un fichier ratings.json :
scrapy crawl ratings -O ratings.json
"""

class ImdbSpider2(scrapy.Spider):
    name = 'ratings'
    # Chargement des datas du json
    with open('imdb.json') as f:
      data = json.load(f)

    # Chargement des URLs à scrapper
    start_urls = [obj['Url'] for obj in data]
    allowed_domains = ['imdb.com']

    # Set the user agent in the spider's custom settings
    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.3'
    }

    def __init__(self, *args, **kwargs):
      super(ImdbSpider2, self).__init__(*args, **kwargs)

      options = Options()
      options.add_argument("--disable-extensions")
      # options.add_argument("--headless")
      options.add_argument("--start-maximized")
      options.add_argument('--ignore-certificate-errors')
      options.add_argument('--ignore-ssl-errors')
      options.add_argument("--disable-gpu")
      options.add_argument("--no-sandbox")
      options.add_argument("--disable-3d-apis") #pour les erreurs GPU stall due to ReadPixels
      options.add_argument("--start-maximized") #important on doit utiliser cette option car autrement certains elements sont non clickables
      options.add_argument('--window-size=1920x1080') #indispensable pour que les elements soient clickables
      service = Service('chromedriver.exe') #Pour piloter Chrome avec selenium
      self.driver = webdriver.Chrome(service=service, options=options)
    
    def parse(self, response):
        self.driver.get(response.url)
        # Attente du chargement de la page (inutile ?)
        # time.sleep(3)

        # Scrap des informations recherchées dans ces pages
        name = self.driver.find_element(By.CLASS_NAME, "sc-a885edd8-9").text
        imdbRating = self.driver.find_element(By.CLASS_NAME, "sc-5931bdee-1").text
        voteCount = self.driver.find_element(By.CLASS_NAME, "sc-5931bdee-3").text
        unweightedMean = self.driver.find_element(By.CLASS_NAME, "sc-d546428c-1").text
        note10 = self.driver.find_element(By.ID, "chart-bar-1-labels-0").text
        note9 = self.driver.find_element(By.ID, "chart-bar-1-labels-1").text
        note8 = self.driver.find_element(By.ID, "chart-bar-1-labels-2").text
        note7 = self.driver.find_element(By.ID, "chart-bar-1-labels-3").text
        note6 = self.driver.find_element(By.ID, "chart-bar-1-labels-4").text
        note5 = self.driver.find_element(By.ID, "chart-bar-1-labels-5").text
        note4 = self.driver.find_element(By.ID, "chart-bar-1-labels-6").text
        note3 = self.driver.find_element(By.ID, "chart-bar-1-labels-7").text
        note2 = self.driver.find_element(By.ID, "chart-bar-1-labels-8").text
        note1 = self.driver.find_element(By.ID, "chart-bar-1-labels-9").text

        # Envoi des informations au pipeline
        yield {
                'name': name,
                'imdbRating': imdbRating,
                'voteCount': voteCount,
                'unweightedMean': unweightedMean,
                'note10': note10,
                'note9': note9,
                'note8': note8,
                'note7': note7,
                'note6': note6,
                'note5': note5,
                'note4': note4,
                'note3': note3,
                'note2': note2,
                'note1': note1
              }