import scrapy
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

"""
Utilise Scrapy avec Selenium pour manipuler le navigateur Chrome pour scraper 
des informations sur chaque film de la page top250 de l'Internet Movie Database.
NB : utilisation pour sauvegarder le yield dans un fichier imdb.json :
scrapy crawl imdb -O imdb.json (NB : se mettre à la racine du projet, imdb_scrap\imdbscrap)
NB : https://googlechromelabs.github.io/chrome-for-testing/ pour les chromedrivers
"""

class ImdbSpider(scrapy.Spider):
    name = 'imdb'
    start_urls = ['https://www.imdb.com/chart/top/']
    allowed_domains = ['imdb.com']

    # Set the user agent in the spider's custom settings
    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.3'
    }

    def __init__(self, *args, **kwargs):
      super(ImdbSpider, self).__init__(*args, **kwargs)

      options = Options()
      options.add_argument("--disable-extensions")
      # options.add_argument("--headless") #empêche l'affichage du navigateur
      options.add_argument("--start-maximized")
      options.add_argument('--ignore-certificate-errors')
      options.add_argument('--ignore-ssl-errors')
      options.add_argument("--disable-gpu")
      options.add_argument("--no-sandbox")
      options.add_argument("--disable-3d-apis") #pour les erreurs GPU stall due to ReadPixels
      options.add_argument("--start-maximized") #important on doit utiliser cette option car autrement certains elements sont non clickables
      options.add_argument('--window-size=1920x1080') #indispensable pour que les elements soient clickables
      service = Service('chromedriver.exe') #Pour piloter Chrome avec selenium
      self.driver = webdriver.Chrome(service=service, options=options)

    def parse(self, response):
        self.driver.get(response.url)
        # Attente du chargement de la page (inutile ?)
        # time.sleep(3)

        # Mise en mémoire des divs contenant les informations des films
        ParentDivs = self.driver.find_elements(By.XPATH, "//li[@class='ipc-metadata-list-summary-item sc-10233bc-0 iherUv cli-parent']")

        for ParentDiv in ParentDivs:
            # Scrap des informations recherchées dans ces divs
            MovieFull = ParentDiv.find_element(By.CLASS_NAME, "ipc-title-link-wrapper").text
            Position, MovieName = MovieFull.split(". ", 1)
            NoteFull = ParentDiv.find_element(By.CLASS_NAME, "ratingGroup--imdb-rating").get_attribute('aria-label')
            Note = NoteFull.split(":")[1].strip()
            SpanElements = ParentDiv.find_elements(By.CLASS_NAME, 'sc-b189961a-8.kLaxqf.cli-title-metadata-item')
            Year = SpanElements[0].text
            Duree = SpanElements[1].text
            UrlDetail = ParentDiv.find_element(By.CLASS_NAME, "ipc-title-link-wrapper").get_attribute('href')
            UrlId = UrlDetail.split("/")[-2]
            UrlPoster = ParentDiv.find_element(By.TAG_NAME, "img").get_attribute('src')
            
            # Envoi des informations au pipeline
            yield {
                'MovieName': MovieName,
                'Position': Position,
                'Note': Note,
                'Year': Year,
                'Duree': Duree,
                'UrlId': UrlId,
                'UrlPoster': UrlPoster
            }

        # Fermeture du navigateur
        self.driver.quit()