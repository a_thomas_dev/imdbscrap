import json

"""
Charge les données du fichier "imdb.json" (une liste d'objets contenant chacun des données attenantes à un film de la liste top 250 d'IMDb), et grâce à l'identifiant unique "UrlId" de chaque film (scrappé précédement) il construit pour chacun l'URL complète de la page du détail de ses votes, qu'il enregistre dans l'objet.
utilisation : python create_imdb_urls.py
"""

# Charge les données JSON à partir du fichier 'imdb.json'
with open('imdb.json') as f:
    data = json.load(f)

# Itére chaque objet et ajoute l'URL désirée
for obj in data:
    obj['Url'] = f"https://www.imdb.com/title/{obj['UrlId']}/ratings/"

# Enregistrer les données mises à jour dans le fichier 'imdb.json'
with open('imdb.json', 'w') as f:
    json.dump(data, f, indent=2)