import matplotlib.pyplot as plt
import numpy as np

# utilisation : python plotGraphs.py

m = 25000
C = 7

# Values of R
R_values = np.arange(1, 11)

# Values of v
v_values = [1000000, 500000, 100000, 25000]

# Create a single plot for all v values
plt.figure(figsize=(10, 6))

for v in v_values:
    WR = ((v / (v + m)) * R_values) + ((m / (v + m)) * C)
    
    # Plot the graph for the current value of v
    plt.plot(R_values, WR, label=f'v = {v}')

plt.xlabel('R')
plt.ylabel('WR')
plt.title('Weighted Rank (WR) for Different Values of R and v')
plt.legend()
plt.grid(True)
plt.show()