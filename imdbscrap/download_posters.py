import json
import requests

"""
Download les posters des films avec l'url trouvées dans imdb.json et les mets dans le dossier 'posters'
Pour le cas où imdb bloque l'utilisation directe des url dans la partie front-end
Utilisation : python download_posters.py
"""

with open('imdb.json') as f:
  data = json.load(f)

for obj in data:
  response = requests.get(obj['UrlPoster'])
  if response.status_code == 200:
    with open(f'./posters/{obj["UrlId"]}.jpg', 'wb') as f:
      f.write(response.content)
