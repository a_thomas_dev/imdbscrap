import json

"""
Crée des nouvelles moyennes pour chaque film en ne tenant pas compte des votes pour les notes extrêmes 10 et 1.
Attribue ensuite selon ces moyennes une nouvelle position pour chaque film dans le top 250.
Puis sauvegarde ces données dans 'imdbNewRatings.json.json'.
Utilisation : python createNewAveragesAndPositions.py
"""

def convert_vote_count_to_int(vote_count_str):
    suffixes = {
        'K': 1000,
        'M': 1000000
    }
    try:
        if vote_count_str[-1] in suffixes:
            return int(float(vote_count_str[:-1]) * suffixes[vote_count_str[-1]])
        else:
            return int(vote_count_str)
    except (IndexError, ValueError):
        return None

def calculate_average_rating(nb_votes):
    total_votes = sum(nb_votes)
    average_rating = sum((i+1) * nb_votes[i] for i in range(10)) / total_votes
    return average_rating, total_votes

def calculate_average_rating_sans_10et1(nb_votes):
    total_votes_sans_10et1 = sum(nb_votes[1:-1])
    average_rating_sans_10et1 = sum((i+2) * nb_votes[i+1] for i in range(8)) / total_votes_sans_10et1
    return average_rating_sans_10et1, total_votes_sans_10et1

# Charge imdbScrapData.json
with open('imdbScrapData.json') as f:
    imdb_data = json.load(f)

m = 25000 # Nombre minimum de votes pour être pris en compte
C = 7 # constante de pondération

for i, obj in enumerate(imdb_data):
    v = convert_vote_count_to_int(obj['voteCount'])
    R, _ = calculate_average_rating(obj['nbVotesNotes1to10'])
    redoneImdbWeightedRating = (v / (v + m)) * R + (m / (v + m)) * C

    R_sans_10et1, _ = calculate_average_rating_sans_10et1(obj['nbVotesNotes1to10'])
    weightedRatingSans10et1 = (v / (v + m)) * R_sans_10et1 + (m / (v + m)) * C
    # Pour retrouver un barême allant de 1 à 10 au lieu de 2 à 9
    weightedRatingSans10et1Stretched = (weightedRatingSans10et1 - 2)/7*9 + 1
    
    # Création des items
    new_obj = {
        'name': obj['name'],
        'positionImdb': obj['positionImdb'],
        'unweightedMeanImdb': obj['unweightedMeanImdb'],
        'ratingImdb': obj['ratingImdb'],
        'redoneImdbUnweightedRating': round(R, 3),
        'redoneImdbWeightedRating': round(redoneImdbWeightedRating, 3),
        'unweightedRatingSans10et1': round(R_sans_10et1, 3),
        'weightedRatingSans10et1': round(weightedRatingSans10et1, 3),
        'weightedRatingSans10et1Stretched': round(weightedRatingSans10et1Stretched, 3),
        'nbVotesNotes1to10': obj['nbVotesNotes1to10'],
        'pourcentagesNotes1to10': obj['pourcentagesNotes1to10'],
        'voteCount': obj['voteCount'],
        'year': obj['year'],
        'duree': obj['duree'],
        'urlId': obj['urlId'],
        'urlRatings': obj['urlRatings'],
        'urlMovie': obj['urlMovie'],
        'urlPoster': obj['urlPoster']
    }

    imdb_data[i] = new_obj

# Création de la position dans le classement par redoneImdbWeightedRating
sorted_imdb_data = sorted(imdb_data, key=lambda obj: obj['redoneImdbWeightedRating'], reverse=True)
for i, obj in enumerate(sorted_imdb_data):
    obj['redoneImdbWeightedRatingPosition'] = i + 1

# Création de la position dans le classement par weightedRatingSans10et1Stretched
sorted_imdb_data = sorted(imdb_data, key=lambda obj: obj['weightedRatingSans10et1Stretched'], reverse=True)
for i, obj in enumerate(sorted_imdb_data):
    obj['weightedRatingSans10et1StretchedPosition'] = i + 1

# Ajout des différences de positions entre le classement imdb originel et ces deux nouvelles positions, 
# pour des classements par progression
for obj in imdb_data:
    obj['redoneImdbWeightedRatingPosDiff'] = obj['positionImdb'] - obj['redoneImdbWeightedRatingPosition']
    obj['weightedRatingSans10et1StretchedPosDiff'] = obj['positionImdb'] - obj['weightedRatingSans10et1StretchedPosition']

# Sauvegarde les anciennes et nouvelles datas dans imdbNewRatings.json
# NB : "ensure_ascii=False" pour gérer e.g. les accents français
with open('imdbNewRatings.json', 'w') as f:
    json.dump(imdb_data, f, indent=2, ensure_ascii=False)