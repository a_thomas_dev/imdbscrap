import json

"""
Joint les datas contenues dans les fichiers 'ratingsProcessed.json' et 'imdb.json' dans 'imdbScrapData.json'
utilisation : python join_files.py
"""

# Charge les datas de ratingsProcessed.json 
with open('ratingsProcessed.json') as f:
    ratings_processed_data = json.load(f)

# Charge les datas de imdb.json
with open('imdb.json') as f:
    imdb_data = json.load(f)

# Joint les deux sets de datas
for rating_obj in ratings_processed_data:
    movie_name = rating_obj['name']
    for imdb_obj in imdb_data:
        # S'assure que c'est le même film dans les deux sets de datas
        if 'MovieName' in imdb_obj and imdb_obj['MovieName'] == movie_name:
            # Retire "Note" qui est maintenant en double
            del imdb_obj['Note']

            # Renommage et réarrangement
            imdb_obj['name'] = imdb_obj.pop('MovieName')
            imdb_obj['positionImdb'] = int(imdb_obj.pop('Position'))
            imdb_obj['ratingImdb'] = rating_obj.pop('ratingImdb')
            imdb_obj['unweightedMeanImdb'] = rating_obj.pop('unweightedMeanImdb')
            imdb_obj['nbVotesNotes1to10'] = rating_obj.pop('nbVotesNotes1to10')
            imdb_obj['pourcentagesNotes1to10'] = rating_obj.pop('pourcentagesNotes1to10')
            imdb_obj['voteCount'] = rating_obj.pop('voteCount')
            imdb_obj['year'] = imdb_obj.pop('Year')
            imdb_obj['duree'] = imdb_obj.pop('Duree')
            imdb_obj['urlId'] = imdb_obj.pop('UrlId')
            imdb_obj['urlPoster'] = imdb_obj.pop('UrlPoster')
            imdb_obj['urlRatings'] = imdb_obj.pop('Url')

            # Création de l'URL de la fiche de chaque film
            imdb_obj['urlMovie'] = f"https://www.imdb.com/title/{imdb_obj['urlId']}/"
            break

# Sauvegarde les datas traitées dans imdbScrapData.json
# NB : "ensure_ascii=False" pour gérer e.g. les accents français
with open('imdbScrapData.json', 'w') as f:
    json.dump(imdb_data, f, indent=2, ensure_ascii=False)