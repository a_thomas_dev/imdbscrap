# imdbScrap
![Python](https://img.shields.io/badge/Python-white?style=plastic&logo=python&logoColor=white&color=%233776AB)
![Scrapy](https://img.shields.io/badge/Scrapy-white?style=plastic&logo=scrapy&logoColor=white&color=%2360A839)
![Selenium](https://img.shields.io/badge/Selenium-white?style=plastic&logo=selenium&logoColor=white&color=%2343B02A)

## Contexte
L'idée pour ce projet m'est venue d'une observation que j'avais faite il y a bien longtemps sur le site de review d'anime [Animeka](https://www.animeka.com/) : Il y a pour chaque anime un graphique montrant le nombre de votes pour chaque note possible donnés par les utilisateurs (dans un choix allant de 0 à 10). Un constat saute presque à chaques fois aux yeux : **la plupart des utilisateurs veulent que leur vote influence le plus possible la moyenne et vont donc voter soit 0 soit 10**, par exemple :
  
<!-- ![votes pour l'anime Naruto](https://i.imgur.com/Vy8n7S0.png)   -->
<div align="center">
  <img src="https://i.imgur.com/Vy8n7S0.png" alt="votes pour l'anime Naruto"/>
</div>
Je me suis rappelé ce fait récemment et je me suis demandé si le fameux [Top 250 Movies](https://www.imdb.com/chart/top/) de l'Internet Movie Database ne souffrirait pas un peu de ce phénomène, qui plus est lorsque l'on prend en compte l'influence probable de bots à la solde du business cinématographique.
  
Donc, **qu'adviendrait-il de l'ordre de cette liste si on recalculait la moyenne de chaque film sans prendre en compte les votes 1 et 10** ?
  
Ce projet a pour but de :
- [ ] Utiliser un scrapper web (**Scrapy**) pour récupérer les diverses données de chaque film de cette liste, dont bien sûr le détail des votes.
- [ ] **Refaire une moyenne pour chaque film** en éliminant les votes extrêmes (1 et 10 pour IMDb).
- [ ] Développer ensuite dans un projet annexe une **partie front-end avec React** pour afficher de nouveau ce Top 250 et **voir à quel point l'ordre des films est affecté** par ces changements.

## Scrapy
**Scrapy** est un framework open-source écrit en **Python** permettant de viser des pages web pour en récupérer des données de façon automatisée.
  
De part le faible nombre de données à scrapper, j'ai choisis d'utiliser le middleware **Selenium** avec Scrapy plutôt que d'essayer de mimiquer des requêtes de façon organique avec juste ce dernier -ce qui peut s'avérer complexe face à un site d'importance comme IMDb qui a évidemment en place des défenses contre les utilisateurs non-humains- pour éviter ainsi plus facilement les réponses bloquantes de type 403.
  
Selenium permet de prendre le contrôle d'un navigateur réel pour aller sur chaque page à scraper, et être donc plus furtif (au détriment de la vitesse d'execution, mais cela est insignifiant dans le cadre de ce projet).
  
J'ai utilisé le navigateur Chrome (version 126.0.6478, win64). Selenium a besoin d'un chromedriver de même version pour pouvoir le piloter, dont on peut trouver une liste [ici](https://googlechromelabs.github.io/chrome-for-testing/). Les données utilisées ont été scrapées du site le 07/06/2024.

## Exploitation des données
Recalculer dans un premier temps la note des films pour vérifier la pertinence des données récupérées n'est pas aussi simple qu'on pourrait le penser : Par exemple sur la page du détails des votes du film [Les évadés](https://www.imdb.com/title/tt0111161/ratings), on constate qu'il y a une note nommée "NOTE IMDb" (9.3/10) mais aussi plus bas une note nommée "Moyenne non-pondérée" (9.1/10).
  
La moyenne non-pondérée semble être calculée simplement (somme des multiplications de chaque note par le nombre de vote qui lui est attribué, divisée par la somme du nombre total de votes), puisque je retrouve un chiffre presque toujours similaire une fois celui-ci arrondi au plus proche avec un seul chiffre après la virgule (les rares différences pouvant s'expliquer par le manque de précision des données présentée : Par exemple j'ai scrappé "1.6M" de votes pour la note 10 mais IMDb a forcément utilisé dans son calcul un chiffre plus précis).
  
En ce qui concerne la "NOTE IMDb", en enquétant on retrouve la formule qu'IMDb a utilisé pour calculer la moyenne pondérée, bien que la dernière mise à jour que j'ai trouvé remonte à 2013 au mieux (mais peu importe, pensais-je, puisque son évolution a été mineure jusque là ; je serai donc probablement capable de l'adapter au goût du jour en tatonant un peu) :
  
<div align="center">
  <img src="https://i.imgur.com/2Xb63mA.png" alt="Formule pour le calcul de la moyenne pondérée des notes"/>
</div>
Malheureusement cela ne semble pas être aussi simple : par exemple "Les évadés" a une moyenne pondérée supérieure (9.3) à sa moyenne non-pondérée (9.1), or j'ai tracé des courbes de cette formule de pondération pour plusieurs nombres totaux de votes et pour des moyennes allant de 1 à 10, et l'on constate que cela ne devrait pas être possible :
  
<div align="center">
  <img src="https://i.imgur.com/tnPGCG0.png" alt="évolution de la moyenne pondérée des notes pour différents nombre de votes"/>
</div>
Axe des ordonnées : moyenne non-pondérée. Axe des abscisse : moyenne pondérée
  
Toutes les moyennes non-pondérées du Top250 sont supérieures à 7.8. Or on constate que pour les moyennes non-pondérée allant de 8 à 10, la formule de pondération ne peut que faire baisser la note.
Pour expliquer ces moyennes qui augmentent après pondération, il y a donc en jeu plus que cette formule. On peut supposer qu'IMDb ne prend par exemple pas en compte les votes des utilisateurs dont le compte est trop récent ou qui n'ont pas écrit d'avis sur un nombre suffisant de films, etc. Ne m'étant pas possible d'accéder à ces données aujourd'hui, il faudra donc que je fasse sans la possibilité de reproduire ces moyennes exactement. C'est dommage puisque le but du projet s'en trouve un peu altéré, mais néanmoins l'exercice reste enrichissant.
  
Les résultats sont présentés dans le projet [imdb_front](https://gitlab.com/a_thomas_dev/imdb_front).



